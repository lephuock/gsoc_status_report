---
title: "digiKam: GSoC 2021 Community Bonding"
date: 2021-06-06
author: "Phuoc Khanh LE"

---

Hello reader, I hope you are great in this time. I am Phuoc Khanh LE, an Infomation Student from France. In the program of [Google Summer of Code](https://summerofcode.withgoogle.com/), my proposal to [digiKam Image Quality Sorter algorithms improvement](https://summerofcode.withgoogle.com/projects/?sp-search=phuoc#6025659698118656) is accepted by [KDE community](https://kde.org/).

Community Bonding is the for time for preparation. I spend my first post to describe this important period. Thanks to mentors Gilles Caulier, Maik Qualmann and Thanh Trung Dinh for helping me prepare for the work this summer.

First Task
==========

I have spent these 3 weeks reading the huge codebase of digiKam, preparing environment for coding, and study in depth the algorithm that I am about to implement. This is the [branch](https://invent.kde.org/graphics/digikam/-/tree/gsoc21-image-quality-sorter) that will have my work for GSoC'21.

After reading enough documentation and mathematic, I start preparing a base for testing each functionality of Image Quality Sorter (IQS). I found that it is the most important task in this period. This helps me having a tool to evaluate my future work without thinking further how to test it. There are 2 types of unit test : unit test for each factor (for example : [detectionblur](https://invent.kde.org/graphics/digikam/-/blob/gsoc21-image-quality-sorter/core/tests/imgqsort/detectblur.cpp)) and unit test by pre-defined [use-case](https://invent.kde.org/graphics/digikam/-/blob/gsoc21-image-quality-sorter/core/tests/imgqsort/imgqsorttest.h).

Although these tests can not be passed now, it will be useful in the future.

Next step
=========

Tomorrow, I will start the work as I promised in the proposal, prepare weekly reports, and document these upcoming 10 weeks. Looking forward to great summer with KDE.

Thanks for reading! Cheers!
  

