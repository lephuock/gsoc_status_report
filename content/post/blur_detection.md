---
title: "Blur detection (first week)"
date: 2021-06-13
author: "Phuoc Khanh LE"

---

As presented in my proposal, I start with blur detection in IQS in the first weel. By getting rid of the old implementation, I create a whole new class of blur detector, in order to configure and customize easily my alogrithm.

Overral Idea
============

The figure below designs my algorithm. 
<img
  id="blur_detection_overall idea"
  src="/images/blur_detection_algo.png"
  alt="blur_detection_algo">

As discused in the proposal, there are 2 types of blur : motion blur and defocused blur. Hence, there should be detectors for each one due to the difference between their nature. Defocused blur appears when the edges of object are fuzzy. While motion blur appears when the object or the camera moves when capturing.

As observation, I found that defocused blur is the disappearance of edges, while motion blur is the appearance of many parallel edges in a small part of image. Based on algoritm of edges detection [Laplacian](https://docs.opencv.org/3.4/d5/db5/tutorial_laplace_operator.html) of opencv, I extract an edges map of the image. I used this map tp detect defocused blur and motion blur of the image.

The avantage of Laplacian is its sentivity. It detects not only main edges but also the small edges inside the object. So, I can distinguish sharp region belonged to the object and blurred region of background.

* Defocused detector :

- This edges map is not enough to detect defocused blur. In order to distinguish sharp pixel of edges and blurred pixel, I apply an log filter. (parameter : ordre_log_filtrer)
- Then, I smooth image by the operateur blur and medianblur of open cv to get a blurred map.
- At last, I determine if a specific pixel is blur nor sharp by using a threshold. (parameter : filtrer_defocus)

* Motion blur detector :

- At first, I consider only the clear edges from edges map, by using a threshold. (parameter : edges_filtrer)
- Then, the image is divided into small parts with a pre-defined size (parameter : part_size_motion_blur)
- Then, I apply [Hough Line Transform](https://docs.opencv.org/3.4/d9/db0/tutorial_hough_lines.html) to determine every line in the part. This transformation uses parameter theta_resolution and threshold_hough to find line in an image. Besides, instead of using class Hough transform (cv::HoughLines() ), I use Probabilistic Hough Line Transform (cv::HoughLinesP). HoughLinesP uses more criteria to determine a line than HoughLines includes : min_line_length. This parameter helps to prevent too short line.
- Having lines of a part, it is easy to calculate the angle of them. As mentioned in the proposal, a small part with many parallel lines is a motion blurred part. The parameter min_nb_lines is used to determine if a part can be motion blur. Then, I check if these lines are parallel. There are some tricks to do it :
    + At first, angle alpha and alpha +/- pi are the same in the term of parallelism. Hence, I limit the angle from 0 to pi radian.
    + Secondly, 2 parallel lines have not necessarily same angle degree, but a very proximate value. Hence, I calculate the standard deviation of angle values and  compare it  to parameter max_stddev. If it is smaller, then, the part is motion blurred.
    + At last, this trick has a weekness when the direction of motion is horizontal. That means angles are around 0 and pi, so the variance would be very big while the image is motion blurred. My solution is to bound the intervall of angle only from 0 to (pi - pi / 20). It would solve this case but it is not the best solution.

Demostration
============

I tested with [test images](https://invent.kde.org/graphics/digikam/-/tree/IQS-blurdetection/core/tests/imgqsort/data) in unit test of digikam. These images are some different use-cases for blur detection. For instance, the algoritm can recognize the sharp image like [rock](https://invent.kde.org/graphics/digikam/-/blob/IQS-blurdetection/core/tests/imgqsort/data/blur_rock_1.jpg), or even difficult case like [tree](https://invent.kde.org/graphics/digikam/-/blob/IQS-blurdetection/core/tests/imgqsort/data/blur_tree_1.jpg). It is also success to recognize motion blur by cheking on image like [rock_2](https://invent.kde.org/graphics/digikam/-/blob/IQS-blurdetection/core/tests/imgqsort/data/blur_rock_2.jpg) or [tree_2](https://invent.kde.org/graphics/digikam/-/blob/IQS-blurdetection/core/tests/imgqsort/data/blur_tree_2.jpg). However, the recognition is not very sensitive to jugde them as rejected but only pending. 

However, there are still some hard situations. Image [street](https://invent.kde.org/graphics/digikam/-/blob/IQS-blurdetection/core/tests/imgqsort/data/blur_street_1.jpg) is easy to be recognized as motion blurred because of it nature. Image [sky](https://invent.kde.org/graphics/digikam/-/blob/IQS-blurdetection/core/tests/imgqsort/data/blur_sky_1.jpg) is recognized as defocused blur because of its part sky and cloud.

Difficuty and plan for next week
================================

The project should cover also the artistic image which focus on some specific position and blurs the rest on purpose . It demands an implementation of focus region extractor based on metadata. Moreover, as mentioned above, there is a dead case for the algorithm when the image have background (sky, wall, water, ...). It is resonable because bachground doesn't have edges to detect. It also can not be seen as an blurred object. I will try to recognize this part in the next week.
